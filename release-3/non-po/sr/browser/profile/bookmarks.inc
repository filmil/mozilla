#filter emptyLines

# LOCALIZATION NOTE: The 'en-US' strings in the URLs will be replaced with
# your locale code, and link to your translated pages as soon as they're 
# live.

#define bookmarks_title Забелешке
#define bookmarks_heading Забелешке

# LOCALIZATION NOTE (bookmarks_addons):
# link title for https://en-US.add-ons.mozilla.com/en-US/firefox/bookmarks/
#define bookmarks_addons Добави додатке за забелешке

#define bookmarks_toolbarfolder Алатна трака са забелешкама
#define bookmarks_toolbarfolder_description Додајте забелешке у овај директоријум како би биле приказане у алатној траци за забелешке

# LOCALIZATION NOTE (getting_started):
# link title for http://en-US.www.mozilla.com/en-US/firefox/central/
#define getting_started Како почети

# LOCALIZATION NOTE (latest_headlines):
# link title for the live bookmarks sample, a redirect on
# http://en-US.fxfeeds.mozilla.com/en-US/firefox/headlines.xml
# Changing the redirect is subject to approval of l10n-drivers.
#define latest_headlines Последње вести

# LOCALIZATION NOTE (firefox_heading):
# Firefox links folder name
#define firefox_heading Мозилин фајерфокс (Mozilla Firefox)

# LOCALIZATION NOTE (firefox_help):
# link title for http://en-US.www.mozilla.com/en-US/firefox/help/
#define firefox_help Помоћ и упутства

# LOCALIZATION NOTE (firefox_customize):
# link title for http://en-US.www.mozilla.com/en-US/firefox/customize/
#define firefox_customize Прилагодите фајерфокс

# LOCALIZATION NOTE (firefox_community):
# link title for http://en-US.www.mozilla.com/en-US/firefox/community/
#define firefox_community Прикључите се

# LOCALIZATION NOTE (firefox_about):
# link title for http://en-US.www.mozilla.com/en-US/firefox/about/
#define firefox_about О нама

#unfilter emptyLines
