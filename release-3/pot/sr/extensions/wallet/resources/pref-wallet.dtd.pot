#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/extensions/wallet/resources/pref-wallet.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: pref.wallet.title
msgid "Forms"
msgstr ""

#: walletHeader.caption
msgid "Form Manager"
msgstr ""

#: walletDescription.label
msgid ""
"Form Manager can remember information about you so that filling out forms on "
"web pages is faster."
msgstr ""

#: walletEnabled.label
#: walletEnabled.accesskey
msgid "Save &form data from web pages when completing forms."
msgstr ""

#: viewWallet.label
#: viewWallet.accesskey
msgid "&Manage Stored Form Data"
msgstr ""

#: viewWalletSites.label
#: viewWalletSites.accesskey
msgid "Manage &Sites"
msgstr ""
