#. # converted from #defines file
#. #filter emptyLines
#. extracted from 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: MOZ_LANGPACK_CREATOR
msgid "The ChatZilla Team"
msgstr ""

#: MOZ_LANGPACK_HOMEPAGE
msgid "http://chatzilla.hacksrus.com/"
msgstr ""

#. # If non-English locales wish to credit multiple contributors, uncomment this
#. # variable definition and use the format specified.
#: MOZ_LANGPACK_CONTRIBUTORS
msgid ""
"<em:contributor>Joe Solon</em:contributor> <em:contributor>Suzy "
"Solon</em:contributor>"
msgstr ""
