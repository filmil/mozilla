#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/other-licenses/branding/firefox/brand.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-01-10 01:53-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: brandShortName
msgid "Firefox"
msgstr ""

#: brandFullName
msgid "Mozilla Firefox"
msgstr ""

#: vendorShortName
msgid "Mozilla"
msgstr ""

#: logoCopyright
msgid ""
"Firefox and the Firefox logos are trademarks of the Mozilla Foundation. All "
"rights reserved."
msgstr ""
