#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/toolkit/chrome/cookie/cookieAcceptDialog.properties
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: hostColon
msgid "Host:"
msgstr ""

#: domainColon
msgid "Domain:"
msgstr ""

#: forSecureOnly
msgid "Encrypted connections only"
msgstr ""

#: forAnyConnection
msgid "Any type of connection"
msgstr ""

#: atEndOfSession
msgid "at end of session"
msgstr ""

#: showDetails
msgid "Show Details"
msgstr ""

#: hideDetails
msgid "Hide Details"
msgstr ""

#: detailsAccessKey
msgid "T"
msgstr ""

#: permissionToSetACookie
msgid "The site %S wants to set a cookie."
msgstr ""

#: permissionToSetSecondCookie
msgid "The site %S wants to set a second cookie."
msgstr ""

#. #  LOCALIZATION NOTE (PermissionToSetAnotherCookie): First %S: sitename, second %S: number of cookies already present for that site
#: permissionToSetAnotherCookie
msgid ""
"The site %S wants to set another cookie.\n"
"You already have %S cookies from this site."
msgstr ""

#: permissionToModifyCookie
msgid "The site %S wants to modify an existing cookie."
msgstr ""
