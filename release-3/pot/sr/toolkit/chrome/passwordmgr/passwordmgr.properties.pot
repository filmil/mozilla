#. # ***** BEGIN LICENSE BLOCK *****
#. # Version: MPL 1.1/GPL 2.0/LGPL 2.1
#. #
#. # The contents of this file are subject to the Mozilla Public License Version
#. # 1.1 (the "License"); you may not use this file except in compliance with
#. # the License. You may obtain a copy of the License at
#. # http://www.mozilla.org/MPL/
#. #
#. # Software distributed under the License is distributed on an "AS IS" basis,
#. # WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
#. # for the specific language governing rights and limitations under the
#. # License.
#. #
#. # The Original Code is Mozilla Password Manager.
#. #
#. # The Initial Developer of the Original Code is
#. # Brian Ryner.
#. # Portions created by the Initial Developer are Copyright (C) 2003
#. # the Initial Developer. All Rights Reserved.
#. #
#. # Contributor(s):
#. #  Brian Ryner <bryner@brianryner.com>
#. #  Ehsan Akhgari <ehsan.akhgari@gmail.com>
#. #
#. # Alternatively, the contents of this file may be used under the terms of
#. # either the GNU General Public License Version 2 or later (the "GPL"), or
#. # the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
#. # in which case the provisions of the GPL or the LGPL are applicable instead
#. # of those above. If you wish to allow use of your version of this file only
#. # under the terms of either the GPL or the LGPL, and not to allow others to
#. # use your version of this file under the terms of the MPL, indicate your
#. # decision by deleting the provisions above and replace them with the notice
#. # and other provisions required by the GPL or the LGPL. If you do not delete
#. # the provisions above, a recipient may use your version of this file under
#. # the terms of any one of the MPL, the GPL or the LGPL.
#. #
#. # ***** END LICENSE BLOCK *****
#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/toolkit/chrome/passwordmgr/passwordmgr.properties
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: rememberValue
msgid "Use Password Manager to remember this value."
msgstr ""

#: rememberPassword
msgid "Use Password Manager to remember this password."
msgstr ""

#: savePasswordTitle
msgid "Confirm"
msgstr ""

#. # 1st string is product name, 2nd is the username for the login, 3rd is the
#. #   login's hostname. Note that long usernames may be truncated.
#: saveLoginText
msgid "Do you want %1$S to remember the password for \"%2$S\" on %3$S?"
msgstr ""

#. # 1st string is product name, 2nd is the login's hostname
#: saveLoginTextNoUsername
msgid "Do you want %1$S to remember this password on %2$S?"
msgstr ""

#: notNowButtonText
msgid "&Not Now"
msgstr ""

#: notifyBarNotNowButtonText
msgid "Not Now"
msgstr ""

#: notifyBarNotNowButtonAccessKey
msgid "N"
msgstr ""

#: neverForSiteButtonText
msgid "Ne&ver for This Site"
msgstr ""

#: notifyBarNeverForSiteButtonText
msgid "Never for This Site"
msgstr ""

#: notifyBarNeverForSiteButtonAccessKey
msgid "e"
msgstr ""

#: rememberButtonText
msgid "&Remember"
msgstr ""

#: notifyBarRememberButtonText
msgid "Remember"
msgstr ""

#: notifyBarRememberButtonAccessKey
msgid "R"
msgstr ""

#: passwordChangeTitle
msgid "Confirm Password Change"
msgstr ""

#: passwordChangeText
msgid "Would you like to change the stored password for %S?"
msgstr ""

#: passwordChangeTextNoUser
msgid "Would you like to change the stored password for this login?"
msgstr ""

#: notifyBarChangeButtonText
msgid "Change"
msgstr ""

#: notifyBarChangeButtonAccessKey
msgid "C"
msgstr ""

#: notifyBarDontChangeButtonText
msgid "Don't Change"
msgstr ""

#: notifyBarDontChangeButtonAccessKey
msgid "D"
msgstr ""

#: userSelectText
msgid "Please confirm which user you are changing the password for"
msgstr ""

#: hidePasswords
msgid "Hide Passwords"
msgstr ""

#: hidePasswordsAccessKey
msgctxt "hidePasswordsAccessKey"
msgid "P"
msgstr ""

#: showPasswords
msgid "Show Passwords"
msgstr ""

#: showPasswordsAccessKey
msgctxt "showPasswordsAccessKey"
msgid "P"
msgstr ""

#: noMasterPasswordPrompt
msgid "Are you sure you wish to show your passwords?"
msgstr ""

#: removeAllPasswordsPrompt
msgid "Are you sure you wish to remove all passwords?"
msgstr ""

#: removeAllPasswordsTitle
msgid "Remove all passwords"
msgstr ""

#: loginsSpielAll
msgid "Passwords for the following sites are stored on your computer:"
msgstr ""

#: loginsSpielFiltered
msgid "The following passwords match your search:"
msgstr ""
