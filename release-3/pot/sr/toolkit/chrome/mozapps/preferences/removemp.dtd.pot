#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/toolkit/chrome/mozapps/preferences/removemp.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: removePassword.title
msgid "Remove Master Password"
msgstr ""

#: removeInfo.label
msgid "You must enter your current password to proceed:"
msgstr ""

#: removeWarning1.label
msgid ""
"Your Master Password is used to protect sensitive information like site "
"passwords."
msgstr ""

#: removeWarning2.label
msgid ""
"If you remove your Master Password your information will not be protected if "
"your computer is compromised."
msgstr ""

#: setPassword.oldPassword.label
msgid "Current password:"
msgstr ""
