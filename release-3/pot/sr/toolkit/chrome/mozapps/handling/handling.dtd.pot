#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/toolkit/chrome/mozapps/handling/handling.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: window.emWidth
msgctxt "window.emWidth"
msgid "26em"
msgstr ""

#: window.emHeight
msgctxt "window.emHeight"
msgid "26em"
msgstr ""

#: ChooseApp.description
msgid "Choose an Application"
msgstr ""

#: ChooseApp.label
#: ChooseApp.accessKey
msgid "&Choose…"
msgstr ""
