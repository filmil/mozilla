#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/toolkit/chrome/global/downloadProgress.properties
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#. # LOCALIZATION NOTE (BadPluginTitle): 
#. #
#. #    This dialog is displayed when plugin throws unhandled exception
#. #
#: BadPluginTitle
msgid "Illegal Operation in Plug-in"
msgstr ""

#. # LOCALIZATION NOTE (BadPluginMessage): 
#. #
#. #    This is the message for the BadPlugin dialog.
#. #    %S will be replaced by brandShortName.
#. #
#: BadPluginMessage
msgid ""
"The plug-in performed an illegal operation. You are strongly advised to "
"restart %S."
msgstr ""

#. # LOCALIZATION NOTE (BadPluginCheckboxMessage): 
#. #
#. #    This message tells the user that if they check this checkbox, they
#. #    will never see this dialog again.
#. #
#: BadPluginCheckboxMessage
msgid "Don't show this message again during this session."
msgstr ""
