#. # ***** BEGIN LICENSE BLOCK *****
#. # Version: MPL 1.1/GPL 2.0/LGPL 2.1
#. #
#. # The contents of this file are subject to the Mozilla Public License Version
#. # 1.1 (the "License"); you may not use this file except in compliance with
#. # the License. You may obtain a copy of the License at
#. # http://www.mozilla.org/MPL/
#. #
#. # Software distributed under the License is distributed on an "AS IS" basis,
#. # WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
#. # for the specific language governing rights and limitations under the
#. # License.
#. #
#. # The Original Code is the Mozilla Installer code.
#. #
#. # The Initial Developer of the Original Code is Mozilla Foundation
#. # Portions created by the Initial Developer are Copyright (C) 2006
#. # the Initial Developer. All Rights Reserved.
#. #
#. # Contributor(s):
#. #  Robert Strong <robert.bugzilla@gmail.com>
#. #
#. # Alternatively, the contents of this file may be used under the terms of
#. # either the GNU General Public License Version 2 or later (the "GPL"), or
#. # the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
#. # in which case the provisions of the GPL or the LGPL are applicable instead
#. # of those above. If you wish to allow use of your version of this file only
#. # under the terms of either the GPL or the LGPL, and not to allow others to
#. # use your version of this file under the terms of the MPL, indicate your
#. # decision by deleting the provisions above and replace them with the notice
#. # and other provisions required by the GPL or the LGPL. If you do not delete
#. # the provisions above, a recipient may use your version of this file under
#. # the terms of any one of the MPL, the GPL or the LGPL.
#. #
#. # ***** END LICENSE BLOCK *****
#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/browser/installer/mui.properties
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#. # To make the l10n tinderboxen see changes to this file you can change a value
#. # name by adding - to the end of the name followed by chars (e.g. Branding-2).
#. # LOCALIZATION NOTE:
#. # This file must be saved as UTF8
#. # Accesskeys are defined by prefixing the letter that is to be used for the
#. # accesskey with an ampersand (e.g. &).
#. # Do not replace $BrandShortName, $BrandFullName, or $BrandFullNameDA with a
#. # custom string and always use the same one as used by the en-US files.
#. # $BrandFullNameDA allows the string to contain an ampersand (e.g. DA stands
#. # for double ampersand) and prevents the letter following the ampersand from
#. # being used as an accesskey.
#. # You can use \n to create a newline in the string but only when the string
#. # from en-US contains a \n.
#: MUI_TEXT_WELCOME_INFO_TITLE
msgid "Welcome to the $BrandFullNameDA Setup Wizard"
msgstr ""

#: MUI_TEXT_WELCOME_INFO_TEXT
msgid ""
"This wizard will guide you through the installation of $BrandFullNameDA.\n"
"\n"
"It is recommended that you close all other applications before starting "
"Setup. This will make it possible to update relevant system files without "
"having to reboot your computer.\n"
"\n"
"$_CLICK"
msgstr ""

#: MUI_TEXT_COMPONENTS_TITLE
msgid "Choose Components"
msgstr ""

#: MUI_TEXT_COMPONENTS_SUBTITLE
msgid "Choose which features of $BrandFullNameDA you want to install."
msgstr ""

#: MUI_INNERTEXT_COMPONENTS_DESCRIPTION_TITLE
msgid "Description"
msgstr ""

#: MUI_INNERTEXT_COMPONENTS_DESCRIPTION_INFO
msgid "Position your mouse over a component to see its description."
msgstr ""

#: MUI_TEXT_DIRECTORY_TITLE
msgid "Choose Install Location"
msgstr ""

#: MUI_TEXT_DIRECTORY_SUBTITLE
msgid "Choose the folder in which to install $BrandFullNameDA."
msgstr ""

#: MUI_TEXT_INSTALLING_TITLE
msgid "Installing"
msgstr ""

#: MUI_TEXT_INSTALLING_SUBTITLE
msgid "Please wait while $BrandFullNameDA is being installed."
msgstr ""

#: MUI_TEXT_FINISH_TITLE
msgid "Installation Complete"
msgstr ""

#: MUI_TEXT_FINISH_SUBTITLE
msgid "Setup was completed successfully."
msgstr ""

#: MUI_TEXT_ABORT_TITLE
msgid "Installation Aborted"
msgstr ""

#: MUI_TEXT_ABORT_SUBTITLE
msgid "Setup was not completed successfully."
msgstr ""

#: MUI_BUTTONTEXT_FINISH
msgid "&Finish"
msgstr ""

#: MUI_TEXT_FINISH_INFO_TITLE
msgid "Completing the $BrandFullNameDA Setup Wizard"
msgstr ""

#: MUI_TEXT_FINISH_INFO_TEXT
msgid ""
"$BrandFullNameDA has been installed on your computer.\n"
"\n"
"Click Finish to close this wizard."
msgstr ""

#: MUI_TEXT_FINISH_INFO_REBOOT
msgid ""
"Your computer must be restarted in order to complete the installation of "
"$BrandFullNameDA. Do you want to reboot now?"
msgstr ""

#: MUI_TEXT_FINISH_REBOOTNOW
msgid "Reboot now"
msgstr ""

#: MUI_TEXT_FINISH_REBOOTLATER
msgid "I want to manually reboot later"
msgstr ""

#: MUI_TEXT_STARTMENU_TITLE
msgid "Choose Start Menu Folder"
msgstr ""

#: MUI_TEXT_STARTMENU_SUBTITLE
msgid "Choose a Start Menu folder for the $BrandFullNameDA shortcuts."
msgstr ""

#: MUI_INNERTEXT_STARTMENU_TOP
msgid ""
"Select the Start Menu folder in which you would like to create the program's "
"shortcuts. You can also enter a name to create a new folder."
msgstr ""

#: MUI_TEXT_ABORTWARNING
msgid "Are you sure you want to quit $BrandFullName Setup?"
msgstr ""

#: MUI_UNTEXT_WELCOME_INFO_TITLE
msgid "Welcome to the $BrandFullNameDA Uninstall Wizard"
msgstr ""

#: MUI_UNTEXT_WELCOME_INFO_TEXT
msgid ""
"This wizard will guide you through the uninstallation of $BrandFullNameDA.\n"
"\n"
"Before starting the uninstallation, make sure $BrandFullNameDA is not "
"running.\n"
"\n"
"$_CLICK"
msgstr ""

#: MUI_UNTEXT_CONFIRM_TITLE
msgid "Uninstall $BrandFullNameDA"
msgstr ""

#: MUI_UNTEXT_CONFIRM_SUBTITLE
msgid "Remove $BrandFullNameDA from your computer."
msgstr ""

#: MUI_UNTEXT_UNINSTALLING_TITLE
msgid "Uninstalling"
msgstr ""

#: MUI_UNTEXT_UNINSTALLING_SUBTITLE
msgid "Please wait while $BrandFullNameDA is being uninstalled."
msgstr ""

#: MUI_UNTEXT_FINISH_TITLE
msgid "Uninstallation Complete"
msgstr ""

#: MUI_UNTEXT_FINISH_SUBTITLE
msgid "Uninstall was completed successfully."
msgstr ""

#: MUI_UNTEXT_ABORT_TITLE
msgid "Uninstallation Aborted"
msgstr ""

#: MUI_UNTEXT_ABORT_SUBTITLE
msgid "Uninstall was not completed successfully."
msgstr ""

#: MUI_UNTEXT_FINISH_INFO_TITLE
msgid "Completing the $BrandFullNameDA Uninstall Wizard"
msgstr ""

#: MUI_UNTEXT_FINISH_INFO_TEXT
msgid ""
"$BrandFullNameDA has been uninstalled from your computer.\n"
"\n"
"Click Finish to close this wizard."
msgstr ""

#: MUI_UNTEXT_FINISH_INFO_REBOOT
msgid ""
"Your computer must be restarted in order to complete the uninstallation of "
"$BrandFullNameDA. Do you want to reboot now?"
msgstr ""

#: MUI_UNTEXT_ABORTWARNING
msgid "Are you sure you want to quit $BrandFullName Uninstall?"
msgstr ""
