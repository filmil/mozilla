#. # ***** BEGIN LICENSE BLOCK *****
#. # Version: MPL 1.1/GPL 2.0/LGPL 2.1
#. #
#. # The contents of this file are subject to the Mozilla Public License Version
#. # 1.1 (the "License"); you may not use this file except in compliance with
#. # the License. You may obtain a copy of the License at
#. # http://www.mozilla.org/MPL/
#. #
#. # Software distributed under the License is distributed on an "AS IS" basis,
#. # WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
#. # for the specific language governing rights and limitations under the
#. # License.
#. #
#. # The Original Code is the Mozilla Installer code.
#. #
#. # The Initial Developer of the Original Code is Mozilla Foundation
#. # Portions created by the Initial Developer are Copyright (C) 2006
#. # the Initial Developer. All Rights Reserved.
#. #
#. # Contributor(s):
#. #  Robert Strong <robert.bugzilla@gmail.com>
#. #
#. # Alternatively, the contents of this file may be used under the terms of
#. # either the GNU General Public License Version 2 or later (the "GPL"), or
#. # the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
#. # in which case the provisions of the GPL or the LGPL are applicable instead
#. # of those above. If you wish to allow use of your version of this file only
#. # under the terms of either the GPL or the LGPL, and not to allow others to
#. # use your version of this file under the terms of the MPL, indicate your
#. # decision by deleting the provisions above and replace them with the notice
#. # and other provisions required by the GPL or the LGPL. If you do not delete
#. # the provisions above, a recipient may use your version of this file under
#. # the terms of any one of the MPL, the GPL or the LGPL.
#. #
#. # ***** END LICENSE BLOCK *****
#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/browser/installer/override.properties
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#. # LOCALIZATION NOTE:
#. # This file must be saved as UTF8
#. # Accesskeys are defined by prefixing the letter that is to be used for the
#. # accesskey with an ampersand (e.g. &).
#. # Do not replace $BrandShortName, $BrandFullName, or $BrandFullNameDA with a
#. # custom string and always use the same one as used by the en-US files.
#. # $BrandFullNameDA allows the string to contain an ampersand (e.g. DA stands
#. # for double ampersand) and prevents the letter following the ampersand from
#. # being used as an accesskey.
#. # You can use \n to create a newline in the string but only when the string
#. # from en-US contains a \n.
#. # Strings that require a space at the end should be enclosed with double
#. # quotes and the double quotes will be removed. To add quotes to the beginning
#. # and end of a strong enclose the add and additional double quote to the
#. # beginning and end of the string (e.g. ""This will include quotes"").
#: SetupCaption
msgid "$BrandFullName Setup"
msgstr ""

#: UninstallCaption
msgid "$BrandFullName Uninstall"
msgstr ""

#: BackBtn
msgid "< &Back"
msgstr ""

#: NextBtn
msgid "&Next >"
msgstr ""

#: AcceptBtn
msgid "I &accept the terms in the License Agreement"
msgstr ""

#: DontAcceptBtn
msgid "I &do not accept the terms in the License Agreement"
msgstr ""

#: InstallBtn
msgid "&Install"
msgstr ""

#: UninstallBtn
msgid "&Uninstall"
msgstr ""

#: CancelBtn
msgid "Cancel"
msgstr ""

#: CloseBtn
msgid "&Close"
msgstr ""

#: BrowseBtn
msgid "B&rowse…"
msgstr ""

#: ShowDetailsBtn
msgid "Show &details"
msgstr ""

#: ClickNext
msgid "Click Next to continue."
msgstr ""

#: ClickInstall
msgid "Click Install to start the installation."
msgstr ""

#: ClickUninstall
msgid "Click Uninstall to start the uninstallation."
msgstr ""

#: Completed
msgid "Completed"
msgstr ""

#: LicenseTextRB
msgid ""
"Please review the license agreement before installing $BrandFullNameDA. If "
"you accept all terms of the agreement, select the first option below. "
"$_CLICK"
msgstr ""

#: ComponentsText
msgid ""
"Check the components you want to install and uncheck the components you "
"don't want to install. $_CLICK"
msgstr ""

#: ComponentsSubText2_NoInstTypes
msgid "Select components to install:"
msgstr ""

#: DirText
msgid ""
"Setup will install $BrandFullNameDA in the following folder. To install in a "
"different folder, click Browse and select another folder. $_CLICK"
msgstr ""

#: DirSubText
msgid "Destination Folder"
msgstr ""

#: DirBrowseText
msgid "Select the folder to install $BrandFullNameDA in:"
msgstr ""

#: SpaceAvailable
msgid "\"Space available: \""
msgstr ""

#: SpaceRequired
msgid "\"Space required: \""
msgstr ""

#: UninstallingText
msgid "$BrandFullNameDA will be uninstalled from the following folder. $_CLICK"
msgstr ""

#: UninstallingSubText
msgid "Uninstalling from:"
msgstr ""

#: FileError
msgid ""
"Error opening file for writing: \\r\n"
"\\r\n"
"$0\\r\n"
"\\r\n"
"Click Abort to stop the installation,\\r\n"
"Retry to try again, or\\r\n"
"Ignore to skip this file."
msgstr ""

#: FileError_NoIgnore
msgid ""
"Error opening file for writing: \\r\n"
"\\r\n"
"$0\\r\n"
"\\r\n"
"Click Retry to try again, or\\r\n"
"Cancel to stop the installation."
msgstr ""

#: CantWrite
msgid "\"Can't write: \""
msgstr ""

#: CopyFailed
msgid "Copy failed"
msgstr ""

#: CopyTo
msgid "\"Copy to \""
msgstr ""

#: Registering
msgid "\"Registering: \""
msgstr ""

#: Unregistering
msgid "\"Unregistering: \""
msgstr ""

#: SymbolNotFound
msgid "\"Could not find symbol: \""
msgstr ""

#: CouldNotLoad
msgid "\"Could not load: \""
msgstr ""

#: CreateFolder
msgid "\"Create folder: \""
msgstr ""

#: CreateShortcut
msgid "\"Create shortcut: \""
msgstr ""

#: CreatedUninstaller
msgid "\"Created uninstaller: \""
msgstr ""

#: Delete
msgid "\"Delete file: \""
msgstr ""

#: DeleteOnReboot
msgid "\"Delete on reboot: \""
msgstr ""

#: ErrorCreatingShortcut
msgid "\"Error creating shortcut: \""
msgstr ""

#: ErrorCreating
msgid "\"Error creating: \""
msgstr ""

#: ErrorDecompressing
msgid "Error decompressing data! Corrupted installer?"
msgstr ""

#: ErrorRegistering
msgid "Error registering DLL"
msgstr ""

#: ExecShell
msgid "\"ExecShell: \""
msgstr ""

#: Exec
msgid "\"Execute: \""
msgstr ""

#: Extract
msgid "\"Extract: \""
msgstr ""

#: ErrorWriting
msgid "\"Extract: error writing to file \""
msgstr ""

#: InvalidOpcode
msgid "Installer corrupted: invalid opcode"
msgstr ""

#: NoOLE
msgid "\"No OLE for: \""
msgstr ""

#: OutputFolder
msgid "\"Output folder: \""
msgstr ""

#: RemoveFolder
msgid "\"Remove folder: \""
msgstr ""

#: RenameOnReboot
msgid "\"Rename on reboot: \""
msgstr ""

#: Rename
msgid "\"Rename: \""
msgstr ""

#: Skipped
msgid "\"Skipped: \""
msgstr ""

#: CopyDetails
msgid "Copy Details To Clipboard"
msgstr ""

#: LogInstall
msgid "Log install process"
msgstr ""

#: Byte
msgid "B"
msgstr ""

#: Kilo
msgid "K"
msgstr ""

#: Mega
msgid "M"
msgstr ""

#: Giga
msgid "G"
msgstr ""
