#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/browser/chrome/browser/aboutRights.properties
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-03-29 12:40-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.3.0\n"
"X-Accelerator-Marker: &\n"

#: buttonLabel
msgid "Know your rights…"
msgstr ""

#: buttonAccessKey
msgid "K"
msgstr ""

#: notifyRightsText
msgid ""
"%S is free and open source software from the non-profit Mozilla Foundation."
msgstr ""
