#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/browser/chrome/browser/preferences/applicationManager.properties
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#. # LOCALIZATION NOTE
#. # in descriptionApplications, %S will be replaced by one of the 3 following strings
#: descriptionApplications
msgid "The following applications can be used to handle %S."
msgstr ""

#: handleProtocol
msgid "%S links"
msgstr ""

#: handleWebFeeds
msgid "Web Feeds"
msgstr ""

#: handleFile
msgid "%S content"
msgstr ""

#: descriptionWebApp
msgid "This web application is hosted at:"
msgstr ""

#: descriptionLocalApp
msgid "This application is located at:"
msgstr ""
