#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/browser/chrome/browser/places/editBookmarkOverlay.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: editBookmarkOverlay.name.label
#: editBookmarkOverlay.name.accesskey
msgid "&Name:"
msgstr ""

#: editBookmarkOverlay.location.label
#: editBookmarkOverlay.location.accesskey
msgid "&Location:"
msgstr ""

#: editBookmarkOverlay.feedLocation.label
#: editBookmarkOverlay.feedLocation.accesskey
msgid "&Feed Location:"
msgstr ""

#: editBookmarkOverlay.siteLocation.label
#: editBookmarkOverlay.siteLocation.accesskey
msgid "&Site Location:"
msgstr ""

#: editBookmarkOverlay.liveTitlesSeparator.label
msgid "Live Titles"
msgstr ""

#: editBookmarkOverlay.folder.label
msgid "Folder:"
msgstr ""

#: editBookmarkOverlay.foldersExpanderDown.tooltip
msgid "Show all the bookmarks folders"
msgstr ""

#: editBookmarkOverlay.expanderUp.tooltip
msgid "Hide"
msgstr ""

#: editBookmarkOverlay.tags.label
#: editBookmarkOverlay.tags.accesskey
msgid "&Tags:"
msgstr ""

#: editBookmarkOverlay.tagsEmptyDesc.label
msgid "Separate tags with commas"
msgstr ""

#: editBookmarkOverlay.description.label
#: editBookmarkOverlay.description.accesskey
msgid "&Description:"
msgstr ""

#: editBookmarkOverlay.keyword.label
#: editBookmarkOverlay.keyword.accesskey
msgid "&Keyword:"
msgstr ""

#: editBookmarkOverlay.tagsExpanderDown.tooltip
msgid "Show all tags"
msgstr ""

#: editBookmarkOverlay.loadInSidebar.label
#: editBookmarkOverlay.loadInSidebar.accesskey
msgid "Load t&his bookmark in the sidebar"
msgstr ""

#: editBookmarkOverlay.choose.label
msgid "Choose…"
msgstr ""

#: editBookmarkOverlay.newFolderButton.label
#: editBookmarkOverlay.newFolderButton.accesskey
msgid "New F&older"
msgstr ""
