#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/browser/chrome/browser/places/places.properties
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-01-10 01:45-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: deleteHost
msgid "Delete all from %S"
msgstr ""

#: deleteDomain
msgid "Delete entire domain %S"
msgstr ""

#: deleteHostNoSelection
msgid "Delete host"
msgstr ""

#: deleteDomainNoSelection
msgid "Delete domain"
msgstr ""

#: load-js-data-url-error
msgid ""
"For security reasons, javascript or data urls cannot be loaded from the "
"history window or sidebar."
msgstr ""

#: noTitle
msgid "(no title)"
msgstr ""

#: bookmarksMenuName
msgid "Bookmarks Menu"
msgstr ""

#: bookmarksToolbarName
msgid "Bookmarks Toolbar"
msgstr ""

#: bookmarksMenuEmptyFolder
msgid "(Empty)"
msgstr ""

#. # LOCALIZATION NOTE (bookmarksBackupFilename) :
#. # %S will be replaced by the current date in ISO 8601 format, YYYY-MM-DD.
#. # The resulting string will be suggested as a filename, so make sure that you're
#. # only using characters legal for file names. Consider falling back to the
#. # en-US value if you have to use non-ascii characters.
#: bookmarksBackupFilename
msgid "Bookmarks %S.html"
msgstr ""

#: bookmarksBackupFilenameJSON
msgid "Bookmarks %S.json"
msgstr ""

#: bookmarksBackupTitle
msgid "Bookmarks backup filename"
msgstr ""

#: bookmarksRestoreAlertTitle
msgid "Revert Bookmarks"
msgstr ""

#: bookmarksRestoreAlert
msgid ""
"This will replace all of your current bookmarks with the backup. Are you "
"sure?"
msgstr ""

#: bookmarksRestoreAlertTags
msgid ""
"This will replace all of your current bookmarks and tags with the backup. "
"Are you sure?"
msgstr ""

#: bookmarksRestoreTitle
msgid "Select a bookmarks backup"
msgstr ""

#: bookmarksRestoreFilterName
msgid "JSON"
msgstr ""

#: bookmarksRestoreFilterExtension
msgid "*.json"
msgstr ""

#: bookmarksRestoreFormatError
msgid "Unsupported file type."
msgstr ""

#: bookmarksRestoreParseError
msgid "Unable to process the backup file."
msgstr ""

#: bookmarksLivemarkLoading
msgid "Live Bookmark loading…"
msgstr ""

#: bookmarksLivemarkFailed
msgid "Live Bookmark feed failed to load."
msgstr ""

#: headerTextPrefix1
msgid "Showing"
msgstr ""

#: headerTextPrefix2
msgid "Search Results for"
msgstr ""

#: headerTextPrefix3
msgid "Advanced Search"
msgstr ""

#: lessCriteria.label
msgid "-"
msgstr ""

#: moreCriteria.label
msgid "+"
msgstr ""

#: menuOpenLivemarkOrigin.label
msgid "Open \"%S\""
msgstr ""

#: defaultGroupOnLabel
msgid "Group by Site"
msgstr ""

#: defaultGroupOnAccesskey
msgid "S"
msgstr ""

#: defaultGroupOffLabel
msgctxt "defaultGroupOffLabel"
msgid "No Grouping"
msgstr ""

#: defaultGroupOffAccesskey
msgctxt "defaultGroupOffAccesskey"
msgid "N"
msgstr ""

#: livemarkGroupOnLabel
msgid "Group By Feed"
msgstr ""

#: livemarkGroupOnAccesskey
msgid "F"
msgstr ""

#: livemarkGroupOffLabel
msgctxt "livemarkGroupOffLabel"
msgid "No Grouping"
msgstr ""

#: livemarkGroupOffAccesskey
msgctxt "livemarkGroupOffAccesskey"
msgid "N"
msgstr ""

#: livemarkReload
msgid "Reload"
msgstr ""

#: livemarkReloadAll
msgid "Reload All Live Bookmarks"
msgstr ""

#: livemarkReloadOne
msgid "Reload %S"
msgstr ""

#: sortByName
msgid "Sort '%S' by Name"
msgstr ""

#: sortByNameGeneric
msgctxt "sortByNameGeneric"
msgid "Sort by Name"
msgstr ""

#: view.sortBy.name.label
msgctxt "view.sortBy.name.label"
msgid "Sort by Name"
msgstr ""

#: view.sortBy.name.accesskey
msgctxt "view.sortBy.name.accesskey"
msgid "N"
msgstr ""

#: view.sortBy.url.label
msgid "Sort by Location"
msgstr ""

#: view.sortBy.url.accesskey
msgctxt "view.sortBy.url.accesskey"
msgid "L"
msgstr ""

#: view.sortBy.date.label
msgid "Sort by Visit Date"
msgstr ""

#: view.sortBy.date.accesskey
msgid "V"
msgstr ""

#: view.sortBy.visitCount.label
msgid "Sort by Visit Count"
msgstr ""

#: view.sortBy.visitCount.accesskey
msgid "C"
msgstr ""

#: view.sortBy.keyword.label
msgid "Sort by Keyword"
msgstr ""

#: view.sortBy.keyword.accesskey
msgid "K"
msgstr ""

#: view.sortBy.description.label
msgid "Sort by Description"
msgstr ""

#: view.sortBy.description.accesskey
msgid "D"
msgstr ""

#: view.sortBy.dateAdded.label
msgid "Sort by Added"
msgstr ""

#: view.sortBy.dateAdded.accesskey
msgid "e"
msgstr ""

#: view.sortBy.lastModified.label
msgid "Sort by Last Modified"
msgstr ""

#: view.sortBy.lastModified.accesskey
msgid "M"
msgstr ""

#: view.sortBy.tags.label
msgid "Sort by Tags"
msgstr ""

#: view.sortBy.tags.accesskey
msgid "T"
msgstr ""

#: searchBookmarks
msgid "Search Bookmarks"
msgstr ""

#: searchHistory
msgid "Search History"
msgstr ""

#: searchCurrentDefault
msgid "Search in '%S'"
msgstr ""

#: findInPrefix
msgid "Find in '%S'…"
msgstr ""

#: tabs.openWarningTitle
msgid "Confirm open"
msgstr ""

#: tabs.openWarningMultipleBranded
msgid ""
"You are about to open %S tabs.  This might slow down %S while the pages are "
"loading.  Are you sure you want to continue?"
msgstr ""

#: tabs.openButtonMultiple
msgid "Open tabs"
msgstr ""

#: tabs.openWarningPromptMeBranded
msgid "Warn me when opening multiple tabs might slow down %S"
msgstr ""

#: status_foldercount
msgid "%S object(s)"
msgstr ""

#: SelectImport
msgid "Import Bookmarks File"
msgstr ""

#: EnterExport
msgid "Export Bookmarks File"
msgstr ""

#: saveSearch.title
msgid "Save Search"
msgstr ""

#: saveSearch.inputLabel
msgid "Name:"
msgstr ""

#: saveSearch.inputDefaultText
msgid "New Search"
msgstr ""

#: detailsPane.noItems
msgid "No items"
msgstr ""

#: detailsPane.oneItem
msgid "One item"
msgstr ""

#: detailsPane.multipleItems
msgid "%S items"
msgstr ""

#: smartBookmarksFolderTitle
msgid "Smart Bookmarks"
msgstr ""

#: mostVisitedTitle
msgid "Most Visited"
msgstr ""

#: recentlyBookmarkedTitle
msgid "Recently Bookmarked"
msgstr ""

#: recentTagsTitle
msgid "Recent Tags"
msgstr ""

#: OrganizerQueryHistory
msgid "History"
msgstr ""

#: OrganizerQueryDownloads
msgid "Downloads"
msgstr ""

#: OrganizerQueryAllBookmarks
msgid "All Bookmarks"
msgstr ""

#. # LOCALIZATION NOTE (tagResultLabel) :
#. # This is what we use to form the label (for screen readers)
#. # for url bar autocomplete results of type "tag"
#. # See createResultLabel() in urlbarBindings.xml
#: tagResultLabel
msgid "Tag"
msgstr ""

#. # LOCALIZATION NOTE (bookmarkResultLabel) :
#. # This is what we use to form the label (for screen readers)
#. # for url bar autocomplete results of type "bookmark"
#. # See createResultLabel() in urlbarBindings.xml
#: bookmarkResultLabel
msgid "Bookmark"
msgstr ""

#. # LOCALIZATION NOTE (lockPrompt.text)
#. # %S will be replaced with the application name.
#: lockPrompt.title
msgid "Browser Startup Error"
msgstr ""

#: lockPrompt.text
msgid ""
"The bookmarks and history system will not be functional because one of %S's "
"files is in use by another application. Some security software can cause "
"this problem."
msgstr ""

#: lockPromptInfoButton.label
msgid "Learn More"
msgstr ""

#: lockPromptInfoButton.accessKey
msgctxt "lockPromptInfoButton.accessKey"
msgid "L"
msgstr ""
