#. # ***** BEGIN LICENSE BLOCK *****
#. # Version: MPL 1.1/GPL 2.0/LGPL 2.1
#. #
#. # The contents of this file are subject to the Mozilla Public License Version
#. # 1.1 (the "License"); you may not use this file except in compliance with
#. # the License. You may obtain a copy of the License at
#. # http://www.mozilla.org/MPL/
#. #
#. # Software distributed under the License is distributed on an "AS IS" basis,
#. # WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
#. # for the specific language governing rights and limitations under the
#. # License.
#. #
#. # The Original Code is mozilla.org code.
#. #
#. # The Initial Developer of the Original Code is
#. # Netscape Communications Corporation.
#. # Portions created by the Initial Developer are Copyright (C) 1998
#. # the Initial Developer. All Rights Reserved.
#. #
#. # Contributor(s):
#. #
#. # Alternatively, the contents of this file may be used under the terms of
#. # either the GNU General Public License Version 2 or later (the "GPL"), or
#. # the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
#. # in which case the provisions of the GPL or the LGPL are applicable instead
#. # of those above. If you wish to allow use of your version of this file only
#. # under the terms of either the GPL or the LGPL, and not to allow others to
#. # use your version of this file under the terms of the MPL, indicate your
#. # decision by deleting the provisions above and replace them with the notice
#. # and other provisions required by the GPL or the LGPL. If you do not delete
#. # the provisions above, a recipient may use your version of this file under
#. # the terms of any one of the MPL, the GPL or the LGPL.
#. #
#. # ***** END LICENSE BLOCK *****
#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/mail/chrome/messenger/mime.properties
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#. #
#. # The following are used by libmime to emit header display in HTML
#. #
#. # Out of memory
#. ## @name MIME_OUT_OF_MEMORY
#. ## @loc None
#: -1000
msgid "Application is out of memory."
msgstr ""

#. # Can't open a temp file
#. ## @name MIME_UNABLE_TO_OPEN_TMP_FILE
#. ## @loc None
#. # LOCALIZATION NOTE (-1001): Do not translate "\n%.200s.\n\n" below. It will display the name of the temporary directory
#: -1001
msgid ""
"Unable to open the temporary file\n"
"%.200s.\n"
"\n"
"\\Check your `Temporary Directory' setting and try again."
msgstr ""

#. # Can't write temp file
#. ## @name MIME_ERROR_WRITING_FILE
#. ## @loc None
#: -1002
msgid "Error writing temporary file."
msgstr ""

#. # Mail subject
#. ## @name MIME_MHTML_SUBJECT
#. ## @loc None
#: 1000
msgid "Subject"
msgstr ""

#. # Resent-Comments
#. ## @name MIME_MHTML_RESENT_COMMENTS
#. ## @loc
#: 1001
msgid "Resent-Comments"
msgstr ""

#. # Resent-Date
#. ## @name MIME_MHTML_RESENT_DATE
#. ## @loc
#: 1002
msgid "Resent-Date"
msgstr ""

#. # Resent-Sender
#. ## @name MIME_MHTML_RESENT_SENDER
#. ## @loc
#: 1003
msgid "Resent-Sender"
msgstr ""

#. # Resent-From
#. ## @name MIME_MHTML_RESENT_FROM
#. ## @loc
#: 1004
msgid "Resent-From"
msgstr ""

#. # Resent-To
#. ## @name MIME_MHTML_RESENT_TO
#. ## @loc
#: 1005
msgid "Resent-To"
msgstr ""

#. # Resent-CC
#. ## @name MIME_MHTML_RESENT_CC
#. ## @loc 
#. # LOCALIZATION NOTE (1006): Do not translate "CC" below.
#: 1006
msgid "Resent-CC"
msgstr ""

#. # Date
#. ## @name MIME_MHTML_DATE
#. ## @loc
#: 1007
msgid "Date"
msgstr ""

#. # Sender
#. ## @name MIME_MHTML_SENDER
#. ## @loc
#: 1008
msgid "Sender"
msgstr ""

#. # From
#. ## @name MIME_MHTML_FROM
#. ## @loc
#: 1009
msgid "From"
msgstr ""

#. # Reply-To
#. ## @name MIME_MHTML_REPLY_TO
#. ## @loc
#: 1010
msgid "Reply-To"
msgstr ""

#. # Organization
#. ## @name MIME_MHTML_ORGANIZATION
#. ## @loc
#: 1011
msgid "Organization"
msgstr ""

#. # To
#. ## @name MIME_MHTML_TO
#. ## @loc
#: 1012
msgid "To"
msgstr ""

#. # CC
#. ## @name MIME_MHTML_CC
#. ## @loc 
#. # LOCALIZATION NOTE (1013): Do not translate "CC" below.
#: 1013
msgid "CC"
msgstr ""

#. # Newsgroups
#. ## @name MIME_MHTML_NEWSGROUPS
#. ## @loc
#: 1014
msgid "Newsgroups"
msgstr ""

#. # Followup-To
#. ## @name MIME_MHTML_FOLLOWUP_TO
#. ## @loc
#: 1015
msgid "Followup-To"
msgstr ""

#. # References
#. ## @name MIME_MHTML_REFERENCES
#. ## @loc
#: 1016
msgid "References"
msgstr ""

#. # Name
#. ## @name MIME_MHTML_NAME
#. ## @loc
#: 1017
msgid "Name"
msgstr ""

#. # Type
#. ## @name MIME_MHTML_TYPE
#. ## @loc
#: 1018
msgid "Type"
msgstr ""

#. # Encoding
#. ## @name MIME_MHTML_ENCODING
#. ## @loc
#: 1019
msgid "Encoding"
msgstr ""

#. # Description
#. ## @name MIME_MHTML_DESCRIPTION
#. ## @loc
#: 1020
msgid "Description"
msgstr ""

#. # Message ID
#. ## @name MIME_MHTML_MESSAGE_ID
#. ## @loc
#: 1021
msgid "Message-ID"
msgstr ""

#. # Resent Message ID
#. ## @name MIME_MHTML_RESENT_MESSAGE_ID
#. ## @loc
#: 1022
msgid "Resent-Message-ID"
msgstr ""

#. # BCC
#. ## @name MIME_MHTML_BCC
#. ## @loc
#: 1023
msgid "BCC"
msgstr ""

#. # Download Status
#. ## @name MIME_MHTML_DOWNLOAD_STATUS_HEADER
#. ## @loc
#: 1024
msgid "Download Status"
msgstr ""

#. # Download status not downloaded
#. ## @name MIME_MHTML_DOWNLOAD_STATUS_NOT_DOWNLOADED
#. ## @loc
#: 1025
msgid "Not Downloaded Inline"
msgstr ""

#. # Link to doc
#. ## @name MIME_MSG_LINK_TO_DOCUMENT
#. ## @loc
#: 1026
msgid "Link to Document"
msgstr ""

#. # Get Doc info
#. ## @name MIME_MSG_DOCUMENT_INFO
#. ## @loc
#: 1027
msgid "<B>Document Info:</B>"
msgstr ""

#. # Msg Attachment
#. ## @name MIME_MSG_ATTACHMENT
#. ## @loc
#: 1028
msgid "Attachment"
msgstr ""

#. # Mouseover text
#. ## @name MIME_MSG_ADDBOOK_MOUSEOVER_TEXT
#. ## @loc 
#. # LOCALIZATION NOTE (1030): Do not translate "%s" below. Place "%s" where you wish to appear a name to be
#. # added to the address book.
#: 1030
msgid "Add %s to your Address Book"
msgstr ""

#. # XSender Internal
#. ## @name MIME_MSG_XSENDER_INTERNAL
#. ## @loc
#. # LOCALIZATION NOTE (1031): Only translate the word "Internal" in the line below.
#: 1031
msgid ""
"<B><FONT COLOR=\\042#808080\\042>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Internal<"
"/FONT></B>"
msgstr ""

#. # User wrote
#. ## @name MIME_MSG_X_USER_WROTE
#. ## @loc 
#. # LOCALIZATION NOTE (1032): Do not translate "%s" below.
#. # Place the first %s where you wish the name of the message to appear
#. # Place the second %s where you wish the name of the user to appear
#. # Do not translate "<P>"
#: 1032
msgid "In message %s %s wrote:<P>"
msgstr ""

#. # Msg user wrote
#. ## @name MIME_MSG_USER_WROTE
#. ## @loc 
#. # LOCALIZATION NOTE (1033): Do not translate "%s" below.
#. # Place the %s where you wish the name of the user to appear
#. # Do not translate "<P>"
#: 1033
msgid "%s wrote:<P>"
msgstr ""

#. # No Headers
#. ## @name MIME_MSG_NO_HEADERS
#. ## @loc
#: 1034
msgid "(no headers)"
msgstr ""

#. # No Headers
#. ## @name MIME_MSG_SHOW_ATTACHMENT_PANE
#. ## @loc
#: 1035
msgid "Toggle Attachment Pane"
msgstr ""

#. # Part not downloaded
#. ## @name MIME_MSG_NOT_DOWNLOADED
#. ## @loc
#: 1036
msgid "(Not Downloaded)"
msgstr ""

#. # Partial Message Format 1
#. ## @name MIME_MSG_PARTIAL_FMT_1
#. ## @loc
#. # LOCALIZATION NOTE (1037): In the following line, translate only the word, "Truncated!".
#: 1037
msgid ""
"<P><CENTER><TABLE BORDER CELLSPACING=5 CELLPADDING=10 WIDTH=\"80%%\"><TR><TD "
"ALIGN=CENTER><FONT SIZE=\"+1\"><B>Truncated!</B></FONT><HR>"
msgstr ""

#. # Partial Message Format 2
#. ## @name MIME_MSG_PARTIAL_FMT_2
#. ## @loc
#. # LOCALIZATION NOTE (1038): Translate the following two lines as a single sentence. In the middle of the two sections
#. # there will be a URL. You may translate the text in any order you wish, but the html tags must stay in the same locations.
#. # In particular, the "<B>" tag must begin the first section, which must end with the "<A HREF=" tag. Do not translate the html tag,"<P>"
#: 1038
msgid ""
"<B>This message exceeded the Maximum Message Size set in Account Settings, "
"so we have only downloaded the first few lines from the mail server.<P>Click "
"<A HREF=\""
msgstr ""

#. # Partial Message Format 3
#. ## @name MIME_MSG_PARTIAL_FMT_3
#. ## @loc
#. # LOCALIZATION NOTE (1039): This section must begin with the ">" sign and end with the tags,"</B></TD></TR></TABLE></CENTER>"
#. # Do not translate "</A>" tag.
#: 1039
msgctxt "1039"
msgid ""
"\">here</A> to download the rest of the "
"message.</B></TD></TR></TABLE></CENTER>"
msgstr ""

#. # default attachment name
#. ## @name MIME_MSG_DEFAULT_ATTACHMENT_NAME
#. ## @loc 
#. # LOCALIZATION NOTE (1040): Do not translate "%s" below.
#. # Place the %s where you wish the part number of the attachment to appear
#: 1040
msgid "Part %s"
msgstr ""

#. # default forwarded message prefix
#. ## @name MIME_FORWARDED_MESSAGE_USER_WROTE
#. ## @loc
#: 1041
msgid "-------- Original Message --------"
msgstr ""

#. # Partial Message Format2 1
#. ## @name MIME_MSG_PARTIAL_FMT2_1
#. ## @loc
#. # LOCALIZATION NOTE (1037): In the following line, translate only the words, "Not Downloaded".
#: 1042
msgid ""
"<P><CENTER><TABLE BORDER CELLSPACING=5 CELLPADDING=10 WIDTH=\"80%%\"><TR><TD "
"ALIGN=CENTER><FONT SIZE=\"+1\"><B>Not Downloaded</B></FONT><HR>"
msgstr ""

#. # Partial Message Format2 2
#. ## @name MIME_MSG_PARTIAL_FMT2_2
#. ## @loc
#. # LOCALIZATION NOTE (1038): Translate the following two lines as a single sentence. In the middle of the two sections
#. # there will be a URL. You may translate the text in any order you wish, but the html tags must stay in the same locations.
#. # In particular, the "<B>" tag must begin the first section, which must end with the "<A HREF=" tag. Do not translate the html tag,"<P>"
#: 1043
msgid ""
"<B>Only the headers for this message were downloaded from the mail "
"server.<P>Click <A HREF=\""
msgstr ""

#. # Partial Message Format2 3
#. ## @name MIME_MSG_PARTIAL_FMT_3
#. ## @loc
#. # LOCALIZATION NOTE (1039): This section must begin with the ">" sign and end with the tags,"</B></TD></TR></TABLE></CENTER>"
#. # Do not translate "</A>" tag.
#: 1044
msgctxt "1044"
msgid ""
"\">here</A> to download the rest of the "
"message.</B></TD></TR></TABLE></CENTER>"
msgstr ""
