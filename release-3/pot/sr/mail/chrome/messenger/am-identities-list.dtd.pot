#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/mail/chrome/messenger/am-identities-list.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: identitiesListDesc.label
msgid "Multiple Identities Support"
msgstr ""

#: identitiesListAdd.label
#: identitiesListAdd.accesskey
msgid "&Add…"
msgstr ""

#: identitiesListEdit.label
#: identitiesListEdit.accesskey
msgid "&Edit…"
msgstr ""

#: identitiesListDelete.label
#: identitiesListDelete.accesskey
msgid "&Delete"
msgstr ""
