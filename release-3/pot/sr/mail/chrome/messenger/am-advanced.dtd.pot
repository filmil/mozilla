#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/mail/chrome/messenger/am-advanced.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-02-16 23:13-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#. extracted from am-advanced.xul
#. do not translate "SMTP" in below line
#: smtpServer.label
msgid "Outgoing Server (SMTP) Settings"
msgstr ""

#. do not translate "SMTP" in below line
#: smtpDesc.label
msgid ""
"Although you can specify more than one outgoing server (SMTP), this is only "
"recommended for advanced users. Setting up multiple SMTP servers can cause "
"errors when sending messages."
msgstr ""

#: smtpListAdd.label
#: smtpListAdd.accesskey
msgid "A&dd…"
msgstr ""

#: smtpListEdit.label
#: smtpListEdit.accesskey
msgid "&Edit…"
msgstr ""

#: smtpListDelete.label
#: smtpListDelete.accesskey
msgid "Re&move"
msgstr ""

#: smtpListSetDefault.label
#: smtpListSetDefault.accesskey
msgid "Se&t Default"
msgstr ""

#: serverDescription.label
msgid "Description: "
msgstr ""

#: serverName.label
msgid "Server Name: "
msgstr ""

#: serverPort.label
msgid "Port: "
msgstr ""

#: userName.label
msgid "User Name: "
msgstr ""

#: connectionSecurity.label
msgid "Connection Security: "
msgstr ""

#: useSecureAuthentication.label
msgid "Secure Authentication: "
msgstr ""
