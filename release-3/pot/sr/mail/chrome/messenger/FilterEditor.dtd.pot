#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/mail/chrome/messenger/FilterEditor.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-09-01 00:00-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.4.0\n"
"X-Accelerator-Marker: &\n"

#: window.title
msgid "Filter Rules"
msgstr ""

#: filterName.label
#: filterName.accesskey
msgid "F&ilter name:"
msgstr ""

#: junk.label
msgid "Junk"
msgstr ""

#: notJunk.label
msgid "Not Junk"
msgstr ""

#: lowestPriorityCmd.label
msgid "Lowest"
msgstr ""

#: lowPriorityCmd.label
msgid "Low"
msgstr ""

#: normalPriorityCmd.label
msgid "Normal"
msgstr ""

#: highPriorityCmd.label
msgid "High"
msgstr ""

#: highestPriorityCmd.label
msgid "Highest"
msgstr ""

#: contextDesc.label
#: contextDesc.accesskey
msgid "Apply filter &when:"
msgstr ""

#: contextIncoming.label
msgid "Checking Mail"
msgstr ""

#: contextManual.label
msgid "Manually Run"
msgstr ""

#: contextBoth.label
msgid "Checking Mail or Manually Run"
msgstr ""

#: contextPostPlugin.label
msgid "Checking Mail (after classification)"
msgstr ""

#: contextPostPluginBoth.label
msgid "Checking Mail (after classification) or Manually Run"
msgstr ""

#: filterActionDesc.label
#: filterActionDesc.accesskey
msgid "&Perform these actions:"
msgstr ""

#. New Style Filter Rule Actions
#: moveMessage.label
msgid "Move Message to"
msgstr ""

#: copyMessage.label
msgid "Copy Message to"
msgstr ""

#: forwardTo.label
msgid "Forward Message to"
msgstr ""

#: replyWithTemplate.label
msgid "Reply with Template"
msgstr ""

#: markMessageRead.label
msgid "Mark As Read"
msgstr ""

#: markMessageStarred.label
msgid "Add Star"
msgstr ""

#: setPriority.label
msgid "Set Priority to"
msgstr ""

#: addTag.label
msgid "Tag Message"
msgstr ""

#: setJunkScore.label
msgid "Set Junk Status to"
msgstr ""

#: deleteMessage.label
msgid "Delete Message"
msgstr ""

#: deleteFromPOP.label
msgid "Delete From POP Server"
msgstr ""

#: fetchFromPOP.label
msgid "Fetch From POP Server"
msgstr ""

#: ignoreThread.label
msgid "Ignore Thread"
msgstr ""

#: ignoreSubthread.label
msgid "Ignore Subthread"
msgstr ""

#: watchThread.label
msgid "Watch Thread"
msgstr ""

#: stopExecution.label
msgid "Stop Filter Execution"
msgstr ""
