#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/mail/chrome/messenger/FilterListDialog.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: window.title
msgid "Message Filters"
msgstr ""

#: nameColumn.label
msgid "Filter Name"
msgstr ""

#: activeColumn.label
msgid "Enabled"
msgstr ""

#: newButton.label
#: newButton.accesskey
msgid "&New…"
msgstr ""

#: editButton.label
#: editButton.accesskey
msgid "&Edit…"
msgstr ""

#: deleteButton.label
#: deleteButton.accesskey
msgid "Dele&te"
msgstr ""

#: reorderUpButton.label
#: reorderUpButton.accesskey
msgid "Move &Up"
msgstr ""

#: reorderDownButton.label
#: reorderDownButton.accesskey
msgid "Move &Down"
msgstr ""

#: filterHeader.label
msgid "Enabled filters are run automatically in the order shown below."
msgstr ""

#: filtersForPrefix.label
#: filtersForPrefix.accesskey
msgid "&Filters for:"
msgstr ""

#: viewLogButton.label
#: viewLogButton.accesskey
msgid "Filter &Log"
msgstr ""

#: runFilters.label
#: runFilters.accesskey
msgid "&Run Now"
msgstr ""

#: stopFilters.label
#: stopFilters.accesskey
msgid "&Stop"
msgstr ""

#: folderPickerPrefix.label
#: folderPickerPrefix.accesskey
msgid "Run sele&cted filter(s) on:"
msgstr ""

#: choosethis.label
msgid "choose this folder"
msgstr ""

#: choosethisnewsserver.label
msgid "choose this news server"
msgstr ""

#: helpButton.label
#: helpButton.accesskey
msgid "&Help"
msgstr ""

#: closeCmd.key
msgid "W"
msgstr ""
