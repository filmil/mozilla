#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/mail/chrome/messenger/am-server-advanced.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: serverAdvanced.label
msgid "Advanced Account Settings"
msgstr ""

#. Do not translate "IMAP"
#: serverDirectory.label
#: serverDirectory.accesskey
msgid "IMAP server &directory:"
msgstr ""

#: usingSubscription.label
#: usingSubscription.accesskey
msgid "Sho&w only subscribed folders"
msgstr ""

#: dualUseFolders.label
#: dualUseFolders.accesskey
msgid "Server supports &folders that contain sub-folders and messages"
msgstr ""

#: useIdle.label
#: useIdle.accesskey
msgid "Use &IDLE command if the server supports it"
msgstr ""

#: maximumConnectionsNumber.label
#: maximumConnectionsNumber.accesskey
msgid "&Maximum number of server connections to cache"
msgstr ""

#. Do not translate "IMAP"
#: namespaceDesc.label
msgid "These preferences specify the namespaces on your IMAP server"
msgstr ""

#: personalNamespace.label
#: personalNamespace.accesskey
msgid "&Personal namespace:"
msgstr ""

#: publicNamespace.label
#: publicNamespace.accesskey
msgid "P&ublic (shared):"
msgstr ""

#: otherUsersNamespace.label
#: otherUsersNamespace.accesskey
msgid "&Other Users:"
msgstr ""

#: overrideNamespaces.label
#: overrideNamespaces.accesskey
msgid "&Allow server to override these namespaces"
msgstr ""

#: pop3Desc.label
msgid ""
"When downloading pop mail for this server, use the following folder for new "
"mail:"
msgstr ""

#: globalInbox.label
#: globalInbox.accesskey
msgid "&Global Inbox (Local Folders Account)"
msgstr ""

#: accountDirectory.label
#: accountDirectory.accesskey
msgid "Inbox for thi&s server's account"
msgstr ""

#: deferToServer.label
#: deferToServer.accesskey
msgid "Inbox for &different account"
msgstr ""

#: deferGetNewMail.label
#: deferGetNewMail.accesskey
msgid "&Include this server when getting new mail"
msgstr ""
