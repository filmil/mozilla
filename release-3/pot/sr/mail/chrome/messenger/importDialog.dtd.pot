#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/mail/chrome/messenger/importDialog.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-02-16 23:24-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#. ***** BEGIN LICENSE BLOCK *****
#. Version: MPL 1.1/GPL 2.0/LGPL 2.1
#. The contents of this file are subject to the Mozilla Public License Version
#. 1.1 (the "License"); you may not use this file except in compliance with
#. the License. You may obtain a copy of the License at
#. http://www.mozilla.org/MPL/
#. Software distributed under the License is distributed on an "AS IS" basis,
#. WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
#. for the specific language governing rights and limitations under the
#. License.
#. The Original Code is Mozilla Communicator client code, released
#. March 31, 1998.
#. The Initial Developer of the Original Code is
#. Netscape Communications Corporation.
#. Portions created by the Initial Developer are Copyright (C) 2001
#. the Initial Developer. All Rights Reserved.
#. Contributor(s):
#. Jeff Beckley <beckley@qualcomm.com>
#. Alternatively, the contents of this file may be used under the terms of
#. either the GNU General Public License Version 2 or later (the "GPL"), or
#. the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
#. in which case the provisions of the GPL or the LGPL are applicable instead
#. of those above. If you wish to allow use of your version of this file only
#. under the terms of either the GPL or the LGPL, and not to allow others to
#. use your version of this file under the terms of the MPL, indicate your
#. decision by deleting the provisions above and replace them with the notice
#. and other provisions required by the GPL or the LGPL. If you do not delete
#. the provisions above, a recipient may use your version of this file under
#. the terms of any one of the MPL, the GPL or the LGPL.
#. ***** END LICENSE BLOCK *****
#. LOCALIZATION NOTE : 'Communicator 4.x' is the used for previous versions of
#. Netscape Communicator, Please translate using the brandname in respective
#. languages for Netscape Communicator 4 releases.
#. LOCALIZATION NOTE : Do not translate any of the occurrences of the word
#. "&brandShortName;" below.
#: importDialog.windowTitle
msgid "Import"
msgstr ""

#: importAll.label
#: importAll.accesskey
msgid "Import &Everything"
msgstr ""

#: importMail.label
#: importMail.accesskey
msgid "&Mail"
msgstr ""

#: importAddressbook.label
#: importAddressbook.accesskey
msgid "&Address Books"
msgstr ""

#: importSettings.label
#: importSettings.accesskey
msgid "&Settings"
msgstr ""

#: importFilters.label
#: importFilters.accesskey
msgid "&Filters"
msgstr ""

#. Do not translate this.  Only change the numeric values if you need this dialogue box to appear bigger
#: window.width
msgid "40em"
msgstr ""

#: window.macWidth
msgid "45em"
msgstr ""

#: importTitle.label
msgid "&brandShortName; Import Wizard"
msgstr ""

#: importShortDesc.label
msgid "Import Mail, Address Books, Settings, and Filters from other programs"
msgstr ""

#: importDescription1.label
msgid ""
"This wizard will import mail messages, address book entries, preferences, "
"and/or filters from other mail programs and common address book formats into "
"&brandShortName;."
msgstr ""

#: importDescription2.label
msgid ""
"Once they have been imported, you will be able to access them from within "
"&brandShortName;."
msgstr ""

#: selectDescription.label
#: selectDescription.accesskey
msgid "&Please select the type of file that you would like to import:"
msgstr ""

#: back.label
msgid "&lt; Back"
msgstr ""

#: forward.label
msgid "Next &gt;"
msgstr ""

#: finish.label
msgid "Finish"
msgstr ""

#: cancel.label
msgid "Cancel"
msgstr ""

#: select.label
msgid "or select the type of material to import:"
msgstr ""

#: title.label
msgid "Title"
msgstr ""

#: processing.label
msgid "Importing…"
msgstr ""

#: FieldDiscInputText1.label
msgid ""
"Netscape Communicator 4.x has one mailing address for each card, while "
"&brandShortName; has two (Home and Work)."
msgstr ""

#: FieldDiscInputText2.label
msgid ""
"Select the category in which you want to store the imported mailing "
"addresses:"
msgstr ""

#: FieldDiscWarning.label
msgid ""
"This category will be used for all the entries of the selected address book: "
""
msgstr ""

#: importHome.label
#: importHome.accesskey
msgid "&Home"
msgstr ""

#: importWork.label
#: importWork.accesskey
msgid "&Work"
msgstr ""
