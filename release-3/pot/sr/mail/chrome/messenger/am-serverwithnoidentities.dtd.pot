#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/mail/chrome/messenger/am-serverwithnoidentities.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: accountTitle.label
msgid "Account Settings"
msgstr ""

#: accountSettingsDesc.label
msgid ""
"The following is a special account.  There are no identities associated with "
"it."
msgstr ""

#: accountName.label
#: accountName.accesskey
msgid "Account &Name:"
msgstr ""

#: messageStorage.label
msgid "Message Storage"
msgstr ""

#: emptyTrashOnExit.label
#: emptyTrashOnExit.accesskey
msgid "Empty Trash on E&xit"
msgstr ""

#: localPath.label
msgid "Local directory:"
msgstr ""

#: localFolderPicker.label
msgid "Select Local Directory"
msgstr ""

#: browseFolder.label
#: browseFolder.accesskey
msgid "&Browse…"
msgstr ""
