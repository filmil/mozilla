#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/mail/chrome/messenger/preferences/notifications.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: notificationsDialog2.title
msgid "Customize New Mail Alert"
msgstr ""

#. Do not translate this.  Only change the numeric values if you need this dialogue box to appear bigger
#: window.width
msgid "30em"
msgstr ""

#: alertCustomization.intro
msgid "Choose which fields to show in the alert notification:"
msgstr ""

#: previewText.label
#: previewText.accesskey
msgid "&Message Preview Text"
msgstr ""

#: subject.label
#: subject.accesskey
msgid "&Subject"
msgstr ""

#: sender.label
#: sender.accesskey
msgid "S&ender"
msgstr ""
