#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/mail/chrome/messenger/preferences/receipts.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#: dialog.title
msgid "Return Receipts"
msgstr ""

#: requestReceipt.label
#: requestReceipt.accesskey
msgid "&When sending messages, always request a return receipt"
msgstr ""

#: receiptArrive.label
msgid "When a receipt arrives:"
msgstr ""

#: leaveIt.label
#: leaveIt.accesskey
msgid "Leave it in my &Inbox"
msgstr ""

#: moveToSent.label
#: moveToSent.accesskey
msgid "&Move it to my &quot;Sent&quot; folder"
msgstr ""

#: requestMDN.label
msgid "When I receive a request for a return receipt:"
msgstr ""

#: never.label
#: never.accesskey
msgid "&Never send a return receipt"
msgstr ""

#: returnSome.label
#: returnSome.accesskey
msgid "Allow &return receipts for some messages"
msgstr ""

#: notInToCc.label
#: notInToCc.accesskey
msgid "I&f I'm not in the To or Cc of the message:"
msgstr ""

#: outsideDomain.label
#: outsideDomain.accesskey
msgid "If &the sender is outside my domain:"
msgstr ""

#: otherCases.label
#: otherCases.accesskey
msgid "In &all other cases:"
msgstr ""

#: askMe.label
msgid "Ask me"
msgstr ""

#: alwaysSend.label
msgid "Always send"
msgstr ""

#: neverSend.label
msgid "Never send"
msgstr ""
