#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/editor/ui/chrome/dialogs/EdAdvancedEdit.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#. ***** BEGIN LICENSE BLOCK *****
#. - Version: MPL 1.1/GPL 2.0/LGPL 2.1
#. -
#. - The contents of this file are subject to the Mozilla Public License Version
#. - 1.1 (the "License"); you may not use this file except in compliance with
#. - the License. You may obtain a copy of the License at
#. - http://www.mozilla.org/MPL/
#. -
#. - Software distributed under the License is distributed on an "AS IS" basis,
#. - WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
#. - for the specific language governing rights and limitations under the
#. - License.
#. -
#. - The Original Code is Mozilla Communicator client code, released
#. - March 31, 1998.
#. -
#. - The Initial Developer of the Original Code is
#. - Netscape Communications Corporation.
#. - Portions created by the Initial Developer are Copyright (C) 1998-1999
#. - the Initial Developer. All Rights Reserved.
#. -
#. - Contributor(s):
#. -   Ben Goodger
#. -
#. - Alternatively, the contents of this file may be used under the terms of
#. - either of the GNU General Public License Version 2 or later (the "GPL"),
#. - or the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
#. - in which case the provisions of the GPL or the LGPL are applicable instead
#. - of those above. If you wish to allow use of your version of this file only
#. - under the terms of either the GPL or the LGPL, and not to allow others to
#. - use your version of this file under the terms of the MPL, indicate your
#. - decision by deleting the provisions above and replace them with the notice
#. - and other provisions required by the GPL or the LGPL. If you do not delete
#. - the provisions above, a recipient may use your version of this file under
#. - the terms of any one of the MPL, the GPL or the LGPL.
#. -
#. - ***** END LICENSE BLOCK *****
#: WindowTitle.label
msgid "Advanced Property Editor"
msgstr ""

#: AttName.label
msgid "Attribute: "
msgstr ""

#: AttValue.label
msgid "Value: "
msgstr ""

#: PropertyName.label
msgid "Property: "
msgstr ""

#: currentattributesfor.label
msgid "Current attributes for: "
msgstr ""

#: tree.attributeHeader.label
msgid "Attribute"
msgstr ""

#: tree.propertyHeader.label
msgid "Property"
msgstr ""

#: tree.valueHeader.label
msgid "Value"
msgstr ""

#: tabHTML.label
msgid "HTML Attributes"
msgstr ""

#: tabCSS.label
msgid "Inline Style"
msgstr ""

#: tabJSE.label
msgid "JavaScript Events"
msgstr ""

#: editAttribute.label
msgid "Click on an item above to edit its value"
msgstr ""

#: removeAttribute.label
msgid "Remove"
msgstr ""
