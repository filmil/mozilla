#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/editor/ui/chrome/dialogs/EditorImageProperties.dtd
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

#. ***** BEGIN LICENSE BLOCK *****
#. - Version: MPL 1.1/GPL 2.0/LGPL 2.1
#. -
#. - The contents of this file are subject to the Mozilla Public License Version
#. - 1.1 (the "License"); you may not use this file except in compliance with
#. - the License. You may obtain a copy of the License at
#. - http://www.mozilla.org/MPL/
#. -
#. - Software distributed under the License is distributed on an "AS IS" basis,
#. - WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
#. - for the specific language governing rights and limitations under the
#. - License.
#. -
#. - The Original Code is Mozilla Communicator client code, released
#. - March 31, 1998.
#. -
#. - The Initial Developer of the Original Code is
#. - Netscape Communications Corporation.
#. - Portions created by the Initial Developer are Copyright (C) 1998-2000
#. - the Initial Developer. All Rights Reserved.
#. -
#. - Contributor(s):
#. -
#. - Alternatively, the contents of this file may be used under the terms of
#. - either of the GNU General Public License Version 2 or later (the "GPL"),
#. - or the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
#. - in which case the provisions of the GPL or the LGPL are applicable instead
#. - of those above. If you wish to allow use of your version of this file only
#. - under the terms of either the GPL or the LGPL, and not to allow others to
#. - use your version of this file under the terms of the MPL, indicate your
#. - decision by deleting the provisions above and replace them with the notice
#. - and other provisions required by the GPL or the LGPL. If you do not delete
#. - the provisions above, a recipient may use your version of this file under
#. - the terms of any one of the MPL, the GPL or the LGPL.
#. -
#. - ***** END LICENSE BLOCK *****
#. These strings are for use specifically in the editor's image and form image dialogs.
#. Window title
#: windowTitle.label
msgid "Image Properties"
msgstr ""

#: pixelsPopup.value
msgid "pixels"
msgstr ""

#. These are in the Location tab panel
#: locationEditField.label
#: locationEditField.accessKey
msgid "Image &Location:"
msgstr ""

#: locationEditField.tooltip
msgid "Type the image's filename or location"
msgstr ""

#: title.label
#: title.accessKey
msgid "&Tooltip:"
msgstr ""

#: title.tooltip
msgid "The html 'title' attribute that displays as a tooltip"
msgstr ""

#: altText.label
#: altText.accessKey
msgid "&Alternate text:"
msgstr ""

#: altTextEditField.tooltip
msgid "Type text to display in place of the image"
msgstr ""

#: noAltText.label
#: noAltText.accessKey
msgid "&Don't use alternate text"
msgstr ""

#: previewBox.label
msgid "Image Preview"
msgstr ""

#. These controls are in the Dimensions tab panel
#. actualSize.label should be same as actualSizeRadio.label + ":"
#: actualSize.label
msgid "Actual Size:"
msgstr ""

#: actualSizeRadio.label
#: actualSizeRadio.accessKey
msgid "&Actual Size"
msgstr ""

#: actualSizeRadio.tooltip
msgid "Revert to the image's actual size"
msgstr ""

#: customSizeRadio.label
#: customSizeRadio.accessKey
msgid "Custom &Size"
msgstr ""

#: customSizeRadio.tooltip
msgid "Change the image's size as displayed in the page"
msgstr ""

#: heightEditField.label
#: heightEditField.accessKey
msgid "Hei&ght:"
msgstr ""

#: widthEditField.label
#: widthEditField.accessKey
msgid "&Width:"
msgstr ""

#: constrainCheckbox.label
#: constrainCheckbox.accessKey
msgid "&Constrain"
msgstr ""

#: constrainCheckbox.tooltip
msgid "Maintain the image's aspect ratio"
msgstr ""

#. These controls are in the Image Map box of the expanded area
#: imagemapBox.label
msgid "Image Map"
msgstr ""

#: removeImageMapButton.label
#: removeImageMapButton.accessKey
msgid "&Remove"
msgstr ""

#: editImageMapButton.label
msgid "Edit…"
msgstr ""

#: editImageMapButton.tooltip
msgid "Create clickable hotspots for this image"
msgstr ""

#. These are the options for image alignment
#: alignment.label
msgid "Align Text to Image"
msgstr ""

#: bottomPopup.value
msgid "At the bottom"
msgstr ""

#: topPopup.value
msgid "At the top"
msgstr ""

#: centerPopup.value
msgid "In the center"
msgstr ""

#: wrapRightPopup.value
msgid "Wrap to the right"
msgstr ""

#: wrapLeftPopup.value
msgid "Wrap to the left"
msgstr ""

#. These controls are in the Spacing Box
#: spacingBox.label
msgid "Spacing"
msgstr ""

#: leftRightEditField.label
#: leftRightEditField.accessKey
msgid "&Left and Right:"
msgstr ""

#: topBottomEditField.label
#: topBottomEditField.accessKey
msgid "&Top and Bottom:"
msgstr ""

#: borderEditField.label
#: borderEditField.accessKey
msgid "Solid &Border:"
msgstr ""

#. These controls are in the Link Box
#: showImageLinkBorder.label
#: showImageLinkBorder.accessKey
msgid "Show &border around linked image"
msgstr ""

#. These tabs are currently used in the image input dialog
#: imageInputTab.label
msgid "Form"
msgstr ""

#: imageLocationTab.label
msgid "Location"
msgstr ""

#: imageDimensionsTab.label
msgid "Dimensions"
msgstr ""

#: imageAppearanceTab.label
msgid "Appearance"
msgstr ""

#: imageLinkTab.label
msgid "Link"
msgstr ""
