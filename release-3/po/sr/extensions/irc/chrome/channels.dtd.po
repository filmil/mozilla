#. extracted from /home/filip/personal/l10n/mozilla/ff-3.1/template/en-US/extensions/irc/chrome/channels.dtd
msgid ""
msgstr ""
"Project-Id-Version: l 10n\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-12-12 19:41-0700\n"
"PO-Revision-Date: 2008-07-05 13:07+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.2.0\n"
"X-Accelerator-Marker: &\n"

# ***** BEGIN LICENSE BLOCK *****
# - Version: MPL 1.1/GPL 2.0/LGPL 2.1
# -
# - The contents of this file are subject to the Mozilla Public License Version
# - 1.1 (the "License"); you may not use this file except in compliance with
# - the License. You may obtain a copy of the License at
# - http://www.mozilla.org/MPL/
# -
# - Software distributed under the License is distributed on an "AS IS" basis,
# - WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
# - for the specific language governing rights and limitations under the
# - License.
# -
# - The Original Code is ChatZilla.
# -
# - The Initial Developer of the Original Code is James Ross.
# - Portions created by the Initial Developer are Copyright (C) 2004
# - the Initial Developer. All Rights Reserved.
# -
# - Contributor(s):
# -   James Ross <silver@warwickcompsoc.co.uk>
# -
# - Alternatively, the contents of this file may be used under the terms of
# - either the GNU General Public License Version 2 or later (the "GPL"), or
# - the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
# - in which case the provisions of the GPL or the LGPL are applicable instead
# - of those above. If you wish to allow use of your version of this file only
# - under the terms of either the GPL or the LGPL, and not to allow others to
# - use your version of this file under the terms of the MPL, indicate your
# - decision by deleting the provisions above and replace them with the notice
# - and other provisions required by the LGPL or the GPL. If you do not delete
# - the provisions above, a recipient may use your version of this file under
# - the terms of any one of the MPL, the GPL or the LGPL.
# -
# - ***** END LICENSE BLOCK *****
#. ***** BEGIN LICENSE BLOCK *****
#. - Version: MPL 1.1/GPL 2.0/LGPL 2.1
#. -
#. - The contents of this file are subject to the Mozilla Public License Version
#. - 1.1 (the "License"); you may not use this file except in compliance with
#. - the License. You may obtain a copy of the License at
#. - http://www.mozilla.org/MPL/
#. -
#. - Software distributed under the License is distributed on an "AS IS" basis,
#. - WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
#. - for the specific language governing rights and limitations under the
#. - License.
#. -
#. - The Original Code is ChatZilla.
#. -
#. - The Initial Developer of the Original Code is James Ross.
#. - Portions created by the Initial Developer are Copyright (C) 2004
#. - the Initial Developer. All Rights Reserved.
#. -
#. - Contributor(s):
#. -   James Ross <silver@warwickcompsoc.co.uk>
#. -
#. - Alternatively, the contents of this file may be used under the terms of
#. - either the GNU General Public License Version 2 or later (the "GPL"), or
#. - the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
#. - in which case the provisions of the GPL or the LGPL are applicable instead
#. - of those above. If you wish to allow use of your version of this file only
#. - under the terms of either the GPL or the LGPL, and not to allow others to
#. - use your version of this file under the terms of the MPL, indicate your
#. - decision by deleting the provisions above and replace them with the notice
#. - and other provisions required by the LGPL or the GPL. If you do not delete
#. - the provisions above, a recipient may use your version of this file under
#. - the terms of any one of the MPL, the GPL or the LGPL.
#. -
#. - ***** END LICENSE BLOCK *****
#: window.title
msgid "Join Channel"
msgstr "Придружи се каналу"

#: qsearch.label
#: qsearch.accesskey
msgid "Quick &Search:"
msgstr "Брза &претрага:"

#: topics.label
#: topics.accesskey
msgid "Search &topics as well as channel names"
msgstr "Тражи &теме као и имена канала"

#: join.label
#: join.accesskey
msgid "&Join"
msgstr "&Придруживање"

#: minusers.label
#: minusers.accesskey
msgid "Mi&n users:"
msgstr "Најма&ње корисника:"

#: maxusers.label
#: maxusers.accesskey
msgid "Ma&x users:"
msgstr "Најви&ше корисника:"

#: refresh.label
msgid "Refresh Now"
msgstr "Освежи сад"

#: refresh.accesskey
msgid ""
"_: refresh.accesskey\n"
""
msgstr "О"

#: col.name
msgid "Name"
msgstr "Име"

#: col.users
msgid "Users"
msgstr "Корисници"

#: col.topic
msgid "Topic"
msgstr "Тема"
